Android Capture App for DHIS 2 (v2.4.3) - Patch version
<table>
<tr> 
<td> 
<img src="https://s3-eu-west-1.amazonaws.com/content.dhis2.org/dhis2-android/android-chrome-384x384.png" width="800"> 
</td> 
<td>
This is a patch version of the <strong>DHIS2 Android App</strong> It builds upon the last version including bug fixes that couldn't wait to the next version. 
It includes no functional improvements neither changes in the User Interface. It means that yours users can update without experiencing any change in the UI. 
</td>
</tr> 
<tr> 
<td colspan="2" bgcolor="white">

## Bugs fixed
* [ANDROAPP-4274](https://jira.dhis2.org/browse/ANDROAPP-4274) Wrong behavior of the selector of org units with open/close dates
* [ANDROAPP-4273](https://jira.dhis2.org/browse/ANDROAPP-4273) App displays incorrect screen in "Notes" tab
* [ANDROAPP-4271](https://jira.dhis2.org/browse/ANDROAPP-4271) App crashes after exiting an event using "Delete and go back" button
* [ANDROAPP-4269](https://jira.dhis2.org/browse/ANDROAPP-4269) Unable to select future dates when scheduling an event
* [ANDROAPP-4267](https://jira.dhis2.org/browse/ANDROAPP-4267) Crash when logout
* [ANDROAPP-4256](https://jira.dhis2.org/browse/ANDROAPP-4256) Sections with indicators only goes into endless loop
* [ANDROAPP-4255](https://jira.dhis2.org/browse/ANDROAPP-4255) No headers for indicator sections
* [ANDROAPP-4254](https://jira.dhis2.org/browse/ANDROAPP-4254) If a render type is used in an attribute with an option set, the registration form does not display any attributes
* [ANDROAPP-4249](https://jira.dhis2.org/browse/ANDROAPP-4249) POSTing TEI with an error in a UNIQUE attribute doesn't show error in the TEI card
* [ANDROAPP-4246](https://jira.dhis2.org/browse/ANDROAPP-4246) Totals don't update values correctly
* [ANDROAPP-4241](https://jira.dhis2.org/browse/ANDROAPP-4241) Cards do not update in the map with only Data Element layer
* [ANDROAPP-4235](https://jira.dhis2.org/browse/ANDROAPP-4235) App doesn't receive updates while syncing TEIs modified in another device
* [ANDROAPP-4117](https://jira.dhis2.org/browse/ANDROAPP-4117) Analytics wrong bottom button
* [ANDROAPP-4107](https://jira.dhis2.org/browse/ANDROAPP-4107) AutoValue_D2Error
* [ANDROAPP-4102](https://jira.dhis2.org/browse/ANDROAPP-4102) DS - Even when the whole row has value, the app keeps asking for values to complete the data set
* [ANDROAPP-4075](https://jira.dhis2.org/browse/ANDROAPP-4075) Maps: TEIs selection doesn't work if there are relationships in the map
* This patch release updates the [Android SDK](https://github.com/dhis2/dhis2-android-sdk) to version 1.4.3.
    
You can find in Jira details on the [bugs fixed](https://jira.dhis2.org/issues/?filter=12099) in this version. 

Remember to check the [documentation](https://www.dhis2.org/android-documentation) for detailed 
information of the features included in the App and how to configure DHIS2 to use it. 

Please create a [Jira](https://jira.dhis2.org/secure/Dashboard.jspa) Issue if you find a bug or 
you want to propose a new functionality. [Project: Android App for DHIS2 | Component: 
AndroidApp].
</td>
</tr>
</table>